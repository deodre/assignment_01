
const FIRST_NAME = "Teodor";
const LAST_NAME = "Cervinski";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(isNaN(value) || value == Infinity || value == -Infinity)
        return NaN;
    else if(value >= Number.MAX_VALUE || value < Number.MIN_VALUE)
            return NaN;
        else return parseInt(value);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

